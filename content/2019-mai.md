+++
title = "mai 2019"
+++

# participants
- Laurent
- Elodie
- William
- Thomas

# ateliers

![ateliers](/journaux/images/ateliers_2019-mai.jpg)

# rétro 

- **William** vraie review des technos autour du machine learning.
  - les différences/spécialités dans les réseaux de neurones
  - les réseaux de neurones semblent être des stats avec un nom plus vendeur
  - pour la suite : passer à l'aspect pratique
- **Elodie** sur la pratique du JavaScript avec [exercism](https://exercism.io) (matin)
  - a revus les bases en JS (`hello world`, structures de contrôle `if/else-if/else`)
  - a eu le retour d'un mentor qui lui a suggéré une autre implémentation
  - les mentors ont l'air bienveillants
  - bcp de bruits dans la syntaxe du langage : "ça ressemble à du C et du Java"
- **Elodie** (après-midi) discussion autour de la [méthodologie XP](https://en.wikipedia.org/wiki/Extreme_programming)
  - peut-être mieux structurer l'activité
  - "c'était quand même chouette !"
  - à approfondir
- **Elodie-Laurent** (après-midi) sur [Pix](https://pix.fr/)
  - rappel de comment faire un test en JavaScript
  - confirmation de la pertinance des propositions de renommages faites en équipe
  - voir en équipe comment adapter le rythme de développement à l'apprentissage
- **Laurent**
  - observe de plus en plus qu'il y a des cheminements standard dans l'activité de développement logiciel (lecture d'une stacktrace, ...)
  - a l'idée de peut-être faire un autre livre sur le sujet
- **Thomas** (matin) sur la découverte de Clojure
  - découverte l'outil de gestion de projet lein
  - découverte de l'obligation d'utilisation des namespace
  - découverte de la logique des imports
  - a bloqué sur la syntaxe functionnelle
  - a quand même envie de continuer
- **William** (après-midi)
 - renommage d'une variable a permis de faire apparaître le sens et a fait comprendre l'algo


